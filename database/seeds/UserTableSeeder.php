<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@ium.edu.na',
            'password' => bcrypt('admin'),
            'active' => 1,
            'role' => 'admin',
        ]);
        DB::table('users')->insert([
            'name' => 'tutor',
            'username' => 'tutor',
            'email' => 'tutor@ium.edu.na',
            'password' => bcrypt('tutor'),
            'active' => 1,
            'role' => 'tutor',
        ]);

        DB::table('users')->insert([
            'name' => 'student',
            'username' => 'student',
            'email' => 'student@ium.edu.na',
            'password' => bcrypt('student'),
            'active' => 1,
            'role' => 'student',
        ]);
    }
}
