@extends('layout.master')
@section('title','Home')
@section('content')
    <section class="section-content">
        <div class="container clearfix">
            <div id="post-318" class="post-318 page type-page status-publish hentry">
                <div id="page-title">
                    <h1 class="entry-title">Log In</h1>
                </div>

                <div class="entry-content">

                    <div id="auth-forms">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="the-tabs">
                            <ul>
                                <li class="active"><a href="{{ route('login') }}">Log in</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            </ul>
                        </div>

                        <div class="login-form clearfix">

                            <form name="loginform" id="loginform" action="{{ route('post_login') }}" method="post">
                                {!! csrf_field() !!}
                                <p class="login-username">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" id="username" class="input" value="" size="20"/>
                                </p>

                                <p class="login-password">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="input" value="" size="20"/>
                                </p>

                                <p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever"/> Remember
                                        Me</label></p>

                                <p class="login-submit">
                                    <input type="submit" name="wp-submit" id="submit-login-form" class="button-primary button-hero" value="Log In"/>
                                    <input type="hidden" name="redirect_to" value="../my-courses/index.html"/>
                                </p>

                            </form>
                            <p class="lost-password-link">
                                <a href="../wp-loginf90c.html?action=lostpassword&amp;redirect_to=http://educator.incrediblebytes.com/login/">Lost
                                    your password?</a>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection