@extends('layout.master')
@section('title','Home')
@section('content')
    <section class="section-content">
        <div class="container clearfix">
            <div id="post-321" class="post-321 page type-page status-publish hentry">
                <div id="page-title">
                    <h1 class="entry-title">Register</h1>
                </div>

                <div class="entry-content">

                    <div id="auth-forms">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="the-tabs">
                            <ul>
                                <li><a href="{{ route('login') }}">Log in</a></li>
                                <li class="active"><a href="{{ route('register') }}">Register</a></li>
                            </ul>
                        </div>


                        <div class="register-form">
                            <form id="registerform" name="registerform" action="{{ route('post_register') }}"
                                  method="post">
                                {!! csrf_field() !!}
                                <p class="login-username">
                                    <label for="name">Full Name</label>
                                    <input type="text" name="name" id="name" class="input" size="20" required value="{{ old('name') }}">
                                </p>

                                <p class="login-username">
                                    <label for="username">Staff/Student Number</label>
                                    <input type="text" name="username" id="username" class="input" size="20" required value="{{ old('username') }}">
                                </p>

                                <p class="login-email">
                                    <label for="register-email">Email</label>
                                    <input type="email" name="email" id="email" class="input" size="20" required value="{{ old('email') }}">
                                </p>
                                <p class="login-password">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="input" value="" size="20"/>
                                </p>
                                <p class="login-password">
                                    <label for="password_again">Repeat Password</label>
                                    <input type="password" name="password_again" id="password_again" class="input" value="" size="20"/>
                                </p>

                                <p class="login-submit">
                                    <input type="submit" name="wp-submit" id="submit-register-form" class="button" value="Register">

                                    <input type="hidden" name="redirect_to" value="indexf634.html?action=registered">
                                </p>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection