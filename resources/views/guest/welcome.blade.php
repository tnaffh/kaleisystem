@extends('layout.master')
@section('title','Home')
@section('content')
    <section class="section-slider">
        <div class="flexslider fw-slider" data-autoscroll="8">
            <ul class="slides">
                <li class="slide overlay active">
                    <div class="slide-image"><img src="wp-content/uploads/2014/10/slide-girl-with-computer-1140x385.jpg"
                                                  sizes="100vw" alt="Educator WP"></div>
                    <div class="slide-caption light left">
                        <div class="container">
                            <div class="caption-inner"><h2 class="slide-title">Online Learning</h2>

                                <div class="slide-description">Facilitate depth understanding of covered topics by the use of online discussion
                                    forums.
                                </div>
                                <div class="buttons"><a class="button button-white" href="#" target="_blank">Browse Courses</a></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="slide overlay">
                    <div class="slide-image"><img src="wp-content/uploads/2014/10/slide-bookshop-1140x385.jpg"
                                                  sizes="100vw" alt="Lorem Ipsum"></div>
                    <div class="slide-caption light left">
                        <div class="container">
                            <div class="caption-inner"><h2 class="slide-title">Study Resources</h2>

                                <div class="slide-description">Get access to unlimited number of study materials - books, lecture notes, videos,
                                    etc for free.
                                </div>
                                <div class="buttons"><a class="button button-white" href="#" target="_blank">Study Materials
                                    </a></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="slide overlay">
                    <div class="slide-image"><img src="wp-content/uploads/2014/10/slide-travel-1140x385.jpg"
                                                  sizes="100vw" alt="Study Anywhere"></div>
                    <div class="slide-caption light left">
                        <div class="container">
                            <div class="caption-inner"><h2 class="slide-title">Study Anywhere</h2>

                                <div class="slide-description">Allow students to learn at their own pace, anywhere, anytime without a need to travel
                                    to a classroom.
                                </div>
                                <div class="buttons"><a class="button button-white" href="#" target="_self">Get Started</a></div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <section class="section-content section-bg" style="background-color:#f5f5f5;">
        <div class="container clearfix">
            <div class="entry-content">
                <div class="dm3-one-fourth">
                    <div class="dm3-box-icon dm3-box-icon-center">
                        <div class="dm3-box-icon-icon"><span class="fa fa-book"></span></div>
                        <div class="dm3-box-icon-content">
                            <h2>Great Material</h2>

                            <p>Students get access to an unlimited number of study materials when they login. Every bit of information you will
                                need is provided to you for free.</p>
                        </div>
                    </div>
                </div>
                <div class="dm3-one-fourth">
                    <div class="dm3-box-icon dm3-box-icon-center">
                        <div class="dm3-box-icon-icon"><span class="fa fa-graduation-cap"></span></div>
                        <div class="dm3-box-icon-content">
                            <h2>High Quality</h2>

                            <p>Get high quality education from professional educators via the system and also through student group discussions and
                                chats.</p>
                        </div>
                    </div>
                </div>
                <div class="dm3-one-fourth">
                    <div class="dm3-box-icon dm3-box-icon-center">
                        <div class="dm3-box-icon-icon"><span class="fa fa-smile-o"></span></div>
                        <div class="dm3-box-icon-content">
                            <h2>Learning is Fun</h2>

                            <p>Learning should not be boring, stressful and tiresome. Learning should be fun as much as posible. Learning though
                                out system is FUN.</p>
                        </div>
                    </div>
                </div>
                <div class="dm3-one-fourth dm3-column-last">
                    <div class="dm3-box-icon dm3-box-icon-center">
                        <div class="dm3-box-icon-icon"><span class="fa fa-check"></span></div>
                        <div class="dm3-box-icon-content">
                            <h2>It is simple</h2>

                            <p>It is simple as abc. It is so simple that you don't need a manual for it. Simple open your web browser and start
                                learning :-)</p>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>




@endsection