<!DOCTYPE html>
<html lang="en-US">
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link type="text/css" media="all" href="{{ asset('wp-content/cache/autoptimize/css/autoptimize_9a790d20eb3fa98db2d7942e22fcca5e.css') }}" rel="stylesheet"/>
    <title>BIS | Online Tutorial - @yield('title')</title>

    {{-- fontawesome --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


</head>
<body class="home page page-id-115 page-template-default has-toolbar">
<div id="page-container">
    <div id="page-toolbar">
        <div class="container clearfix">
            <div class="toolbar-items">
                <div class="item">
                    <div class="inner">
                        Phone: 061 123 456 | contact@ium.edu.na
                    </div>
                </div>

            </div>

            <ul class="toolbar-social">
                <li class="facebook"><a href="https://facebook.com/" title="Facebook" target="_blank"><span class="fa fa-facebook"></span></a></li>
                <li class="google-plus"><a href="https://plus.google.com/" title="Google+" target="_blank"><span class="fa fa-google-plus"></span></a>
                </li>
                <li class="twitter"><a href="https://twitter.com/" title="Twitter" target="_blank"><span class="fa fa-twitter"></span></a></li>
            </ul>
        </div>
    </div>

    <header id="page-header" class="fixed-header">
        <div id="page-header-inner">
            <div id="header-container">
                <div class="container clearfix">
                    <div id="main-logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('wp-content/uploads/2014/12/logo.png') }}" alt=""> </a>
                    </div>

                    @if(\Illuminate\Support\Facades\Auth::check())
                        <ul id="auth-nav">
                            <li><a class="auth-nav-login current_page_item" href="#">{{ \Illuminate\Support\Facades\Auth::user()->username }}</a></li>
                            <li>
                                <div class="auth-nav-register"><a class="button" href="{{ route('logout') }}">Logout</a></div>
                            </li>
                        </ul>
                    @else
                        <ul id="auth-nav">
                            <li><a class="auth-nav-login current_page_item" href="{{ route('login') }}">Log in</a></li>
                            <li>
                                <div class="auth-nav-register"><a class="button" href="{{ route('register') }}">Register</a></div>
                            </li>
                        </ul>
                    @endif

                    @include('layout.main_nav')
                </div>
            </div>
        </div>
    </header>
    @section('content')
    @show
</div>
<!-- #page-container -->

<footer id="page-footer">
    <div class="container clearfix">
        <div class="copy">&copy All rights reserved, Kalei .Inc, 2015</div>
        <button type="button" id="back-to-top"><span class="fa fa-angle-up"></span></button>
        <nav id="footer-nav">
            <ul id="menu-footer-menu" class="menu">
                <li id="menu-item-775"
                    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-115 current_page_item menu-item-775">
                    <a href="/">Home</a></li>
                {{--<li id="menu-item-770" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-770"><a href="courses/index.html">Courses</a>
                </li>
                <li id="menu-item-776" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-776"><a
                            href="#">Blog</a></li>
                <li id="menu-item-788" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-788"><a href="contact-2/index.html">Contact</a>
                </li>--}}
            </ul>
        </nav>
    </div>
</footer>


<script type="text/javascript" defer src="{{ asset('wp-content/cache/autoptimize/js/autoptimize_36108ffdce226e9209e6d3e42d301f83.js') }}"></script>
</body>
</html>
