@extends('layout.master')
@section('title','Tutor - Home')
@section('content')
    <section class="section-content">
        <div class="container clearfix">


            <div id="page-title">
                <h1>Tutor Panel</h1>

            </div>
            {{--
                        <div id="courses-filter" class="clearfix">
                            <div class="drop-down"><a href="#"><span class="text">Category</span><span class="icon"></span></a>
                                <ul>
                                    <li><a href="index.html">All</a></li>
                                    <li><a href="../course-category/business/index.html">Business</a></li>
                                    <li><a href="../course-category/educator/index.html">Educator</a></li>
                                    <li><a href="../course-category/entrepreneurship/index.html">Entrepreneurship</a></li>
                                    <li><a href="../course-category/statistics/index.html">Statistics</a></li>
                                    <li><a href="../course-category/wordpress/index.html">WordPress</a></li>
                                </ul>
                            </div>
                            <div class="drop-down"><a href="#"><span class="text">Membership</span><span class="icon"></span></a>
                                <ul>
                                    <li><a href="index.html">All</a></li>
                                    <li><a href="index1c70.html?membership_id=813">Bronze</a></li>
                                    <li><a href="index760b.html?membership_id=814">Silver</a></li>
                                    <li><a href="index6255.html?membership_id=815">Gold</a></li>
                                </ul>
                            </div>
                            <div class="courses-num">12 course(s)</div>
                        </div>
            --}}
            <div class="posts-grid posts-grid-3 clearfix">

                <article id="post-691"
                         class="post-grid post-691 ib_educator_course type-ib_educator_course status-publish has-post-thumbnail hentry ib_educator_category-entrepreneurship">
                    <div class="post-thumb">
                        <a href="#"><img width="360" height="224" src="http://ium.edu.na/images/2014/tunana.png"
                                         class="attachment-ib-educator-grid wp-post-image" alt="Dream"
                                         sizes="(min-width: 768px) 360px, 96vw"/></a>
                    </div>

                    <div class="post-body">
                        <h2 class="entry-title"><a href="#" rel="bookmark">System Adminstration</a></h2>

                        {{--<div class="price">
                            &#36; 50
                        </div>--}}
                        <div class="post-excerpt"><p>Upload, download, verify uploaded material and complete management of study materials</p>
                        </div>
                    </div>

                    <footer class="post-meta">
                        <span class="difficulty">Administration</span>

                        <div class="share-links-menu"><a href="#" title="Share"><span class="fa fa-bars"></span></a>
                            <ul class="educator-share-links clearfix">
                                <li>
                                    <a href="#"
                                       title="Upload" target="_blank"><span class="fa fa-upload"></span></a></li>
                                <li><a href="#"
                                       title="Delete" target="_blank"><span class="fa fa-trash"></span></a></li>
                                <li>
                                    <a href="#"
                                       title="Manage" target="_blank"><span class="fa fa-cogs"></span></a></li>
                            </ul>
                        </div>
                    </footer>
                </article>
                <article id="post-691"
                         class="post-grid post-691 ib_educator_course type-ib_educator_course status-publish has-post-thumbnail hentry ib_educator_category-entrepreneurship">
                    <div class="post-thumb">
                        <a href="#"><img width="360" height="224" src="http://ium.edu.na/images/2014/tunana.png"
                                         class="attachment-ib-educator-grid wp-post-image" alt="Dream"
                                         sizes="(min-width: 768px) 360px, 96vw"/></a>
                    </div>

                    <div class="post-body">
                        <h2 class="entry-title"><a href="#" rel="bookmark">User Management</a></h2>

                        {{--<div class="price">
                            &#36; 50
                        </div>--}}
                        <div class="post-excerpt"><p>Upload, download, verify uploaded material and complete management of study materials</p>
                        </div>
                    </div>

                    <footer class="post-meta">
                        <span class="difficulty">Administration</span>

                        <div class="share-links-menu"><a href="#" title="Share"><span class="fa fa-bars"></span></a>
                            <ul class="educator-share-links clearfix">
                                <li>
                                    <a href="#"
                                       title="Upload" target="_blank"><span class="fa fa-upload"></span></a></li>
                                <li><a href="#"
                                       title="Delete" target="_blank"><span class="fa fa-trash"></span></a></li>
                                <li>
                                    <a href="#"
                                       title="Manage" target="_blank"><span class="fa fa-cogs"></span></a></li>
                            </ul>
                        </div>
                    </footer>
                </article>
                <article id="post-691"
                         class="post-grid post-691 ib_educator_course type-ib_educator_course status-publish has-post-thumbnail hentry ib_educator_category-entrepreneurship">
                    <div class="post-thumb">
                        <a href="#"><img width="360" height="224" src="http://ium.edu.na/images/2014/tunana.png"
                                         class="attachment-ib-educator-grid wp-post-image" alt="Dream"
                                         sizes="(min-width: 768px) 360px, 96vw"/></a>
                    </div>

                    <div class="post-body">
                        <h2 class="entry-title"><a href="#" rel="bookmark">Study Material</a></h2>

                        {{--<div class="price">
                            &#36; 50
                        </div>--}}
                        <div class="post-excerpt"><p>Upload, download, verify uploaded material and complete management of study materials</p>
                        </div>
                    </div>

                    <footer class="post-meta">
                        <span class="difficulty">Administration</span>

                        <div class="share-links-menu"><a href="#" title="Share"><span class="fa fa-bars"></span></a>
                            <ul class="educator-share-links clearfix">
                                <li>
                                    <a href="#"
                                       title="Upload" target="_blank"><span class="fa fa-upload"></span></a></li>
                                <li><a href="#"
                                       title="Delete" target="_blank"><span class="fa fa-trash"></span></a></li>
                                <li>
                                    <a href="#"
                                       title="Manage" target="_blank"><span class="fa fa-cogs"></span></a></li>
                            </ul>
                        </div>
                    </footer>
                </article>


            </div>

        </div>
    </section>
@endsection