<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    function index(){
        $user = Auth::user();

        switch($user->role){
            case 'admin':
                return view('admin.home');
            break;
            case 'tutor':
                return view('tutor.home');
            break;
            case 'student':
                return view('student.home');
                break;
            default:
                return response([
                    'error' => [
                        'code' => 'INSUFFICIENT_ROLE',
                        'description' => 'You are not authorized to access this resource.'
                    ]
                ], 401);
        }

    }
}
