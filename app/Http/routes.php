<?php

// Authentication routes...
Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'post_login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('auth/register', ['as' => 'post_register', 'uses' => 'Auth\AuthController@postRegister']);


// Password reset link request routes...
Route::get('password/email', ['as' => 'password_email', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'post_password_email', 'uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
Route::get('password/reset/{token}', ['as' => 'password_reset', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('password/reset', ['as' => 'post_password_reset', 'uses' => 'Auth\PasswordController@postReset']);

Route::get('/student', ['as' => 'student', 'middleware' => ['auth','student'], 'uses' => 'StudentController@index']);
Route::get('/admin', ['as' => 'admin', 'middleware' => ['auth','admin'], 'uses' => 'AdminController@index']);
Route::get('/roleindex', ['as' => 'roleindex', 'middleware' => 'auth', 'uses' => 'MasterController@roleIndex']);
Route::get('/tutor', ['as' => 'tutor', 'middleware' => ['auth','tutor'], 'uses' => 'TutorController@index']);
Route::get('/', ['as' => 'home', 'uses' => 'GuestController@index']);
